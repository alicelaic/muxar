package muxar;

import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Created by Alice on 1/24/2016.
 */
@Component
public class ApplicationInit implements ApplicationListener<ContextRefreshedEvent> {
    public static Properties applicationProperties = new Properties();
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        System.out.println("after properties");
        InputStream output = getClass().getClassLoader().getResourceAsStream("application.properties");
        try {
            applicationProperties.load(output);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String getProperty(String key) {
        String value = applicationProperties.getProperty(key);
        if(StringUtils.isEmpty(value)) {
            return "";
        }
        return value;
    }

}
