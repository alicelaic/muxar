package muxar;

import muxar.dto.MusicItem;
import muxar.dto.User;
import muxar.gateway.GatewayService;
import muxar.gateway.dto.MusicRecommendation;
import muxar.gateway.dto.PlayWrapper;
import muxar.security.SecurityService;
import muxar.utils.Constants;
import muxar.utils.session.SessionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.BadRequestException;
import java.util.List;
import java.util.Map;

@Controller
public class HomeController {
	
	@Autowired
	private GatewayService service;


	@Autowired
	private SecurityService securityService;

	@RequestMapping(value ="/")
	public String home(Map<String, Object> model,
							@CookieValue(value = "token", defaultValue = "") String token) {

		User user = SessionUtils.getUser(token);
		if (user != null) {
			return "redirect:/home";
		}else{
			return "login";
		}
	}

	@RequestMapping(value ="/home")
	public String viewStats(Map<String, Object> model,
			@CookieValue(value = "token", defaultValue = "") String token) {

		User user = SessionUtils.getUser(token);
		if (user != null) {
			MusicRecommendation recommendation = service.getRecommendation(user);
			model.put(Constants.RECOMMENDATION_MODEL, recommendation);
			model.put(Constants.MUSIC_FROM_SEARCH, false);
		}

		return "home";
	}

	@RequestMapping(value = "/search", method = RequestMethod.POST)
	public String search(HttpServletRequest request, Map<String, Object> model,@CookieValue(value = "token", defaultValue = "") String token) {
		String searchText = request.getParameter("searchText");

		List<MusicItem> music = service.search( token, searchText);
		MusicRecommendation recommendation = new MusicRecommendation();
		recommendation.setSongsOfArtist(music);
		model.put(Constants.RECOMMENDATION_MODEL, recommendation);
		model.put(Constants.MUSIC_FROM_SEARCH, true);

		return "home";
	}

	@RequestMapping(value = "/play", method = RequestMethod.POST)
	@ResponseBody
	public String play(@RequestParam("songResource") String songResource,

	@RequestParam("songName") String songName,

	@RequestParam("artistResource") String artistResource,

	@RequestParam("artistName") String artistName,

	@RequestParam("artistImage") String artistImage,

	@CookieValue(value = "token", defaultValue = "") String token) {
		if(!securityService.validateToken(token)){
			throw new BadRequestException();
		}

		User user = SessionUtils.getUser(token);
		PlayWrapper play = new PlayWrapper(songResource, songName, artistResource, artistName, artistImage, token, user.getUserId());
		service.play(play);
		return "success";

	}

}