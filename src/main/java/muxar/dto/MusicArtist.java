package muxar.dto;

import com.hp.hpl.jena.graph.Node;
import com.hp.hpl.jena.graph.Triple;
import muxar.utils.Vocabulary;
import muxar.utils.Vocabulary.Property;

import java.io.Serializable;

public class MusicArtist implements Serializable{


	/**
	 * 
	 */
	private static final long serialVersionUID = 3897788812837866207L;
	private String name = "";
	private String image = "";
	private String resourceName = "";

	public MusicArtist() {
	}

	public Triple rdfIsArtist() {
		Node resource = Node.createURI(resourceName);
		Node type = Node.createURI(Vocabulary.RDF_URI + Property.RDF_TYPE);
		Node artistType = Node.createURI(Vocabulary.Type.ARTIST);

		return new Triple(resource, type, artistType);
	}
	
	public Triple rdfHasName() {
		Node artist = Node.createURI(resourceName);
		Node hasName = Node.createURI(Vocabulary.FOAF_URI + Property.FOAF_NAME);
		Node nameValue = Node.createLiteral(name);

		return new Triple(artist, hasName, nameValue);
	}
	
	public Triple rdfHasImage() {
		Node artist = Node.createURI(resourceName);
		Node hasImage = Node.createURI(Vocabulary.SCHEMA_URI + Property.SCHEMA_IMAGE);
		Node imageValue = Node.createURI(image);

		return new Triple(artist, hasImage, imageValue);
	}


	public MusicArtist(String name) {
		this.name = name;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getResourceName() {
		return resourceName;
	}

	public void setResourceName(String resourceName) {
		this.resourceName = resourceName;
	}

}
