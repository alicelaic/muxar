package muxar.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import muxar.utils.Vocabulary;
import muxar.utils.Vocabulary.Property;

import com.hp.hpl.jena.graph.Node;
import com.hp.hpl.jena.graph.Triple;

@JsonIgnoreProperties(ignoreUnknown = true)
public class MusicItem extends MusicRecording {

	public String getUrl() {
		return "http://www.last.fm/music/" + artist.getName() + "/" + name;
	}

	public Triple rdfIsMusicRecording() {
		Node resource = Node.createURI(resourceName);
		Node type = Node.createURI(Vocabulary.RDF_URI + Property.RDF_TYPE);
		Node songType = Node.createURI(Vocabulary.Type.SONG);

		return new Triple(resource, type, songType);
	}
	
	public Triple rdfHasName() {
		
		Node song = Node.createURI(resourceName);
		Node hasName = Node.createURI(Vocabulary.FOAF_URI + Property.FOAF_NAME);
		Node nameValue = Node.createLiteral(name);

		return new Triple(song, hasName, nameValue);
	}
	
	public Triple rdfHasArtist() {
		
		Node song = Node.createURI(resourceName);
		Node hasArtist = Node.createURI(Vocabulary.SCHEMA_URI + Property.SCHEMA_ARTIST);
		Node artistNode = Node.createURI(artist.getResourceName());

		return new Triple(song, hasArtist, artistNode);
	}
}
