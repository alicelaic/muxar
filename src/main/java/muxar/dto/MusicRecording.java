package muxar.dto;

public class MusicRecording {

	protected String resourceName="";
	protected String name="";
	protected MusicArtist artist;

	public MusicRecording() {
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public MusicArtist getArtist() {
		return artist;
	}
	public void setArtist(MusicArtist artist) {
		this.artist = artist;
	}
	public String getResourceName() {
		return resourceName;
	}
	public void setResourceName(String resourceName) {
		this.resourceName = resourceName;
	}

}
