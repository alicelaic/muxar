package muxar.dto;

import muxar.gateway.domain.MusicFacebook;

/**
 * Created by Alice on 1/25/2016.
 */
public class User {
    private String userId;
    private String name;
    private String fbAccessToken;
    private MusicFacebook musicFacebook;

    public User() {
        super();
    }

    public User(String userId, String name, String fbAccessToken) {
        this.userId = userId;
        this.name = name;
        this.fbAccessToken = fbAccessToken;
        		
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFbAccessToken() {
        return fbAccessToken;
    }

    public void setFbAccessToken(String fbAccessToken) {
        this.fbAccessToken = fbAccessToken;
    }

	public MusicFacebook getMusicFacebook() {
		return musicFacebook;
	}

	public void setMusicFacebook(MusicFacebook musicFacebook) {
		this.musicFacebook = musicFacebook;
	}
    
    
}
