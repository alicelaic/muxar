package muxar.facebook.controller;

import muxar.dto.User;
import muxar.facebook.service.FacebookService;
import muxar.gateway.domain.MusicFacebook;
import muxar.security.SecurityService;
import muxar.utils.session.SessionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.InternalServerErrorException;

/**
 * Created by Alice on 1/24/2016.
 */
@Controller
public class FacebookController {

    @Autowired
    private FacebookService facebookService;

    @Autowired
    private SecurityService securityService;

    @RequestMapping(value="/facebook", method = RequestMethod.POST)
    public String createFacebookRelationship(@RequestParam("name") String username, @RequestParam("id") String userId, @RequestParam(value = "accessToken", defaultValue = "") String accessToken, Model model, @CookieValue(value = "token", defaultValue = "") String token, HttpServletRequest request) {
        //persist user first time into session
    	User user=new User(userId, username, accessToken);
        SessionUtils.setUser(userId, username, accessToken,facebookService.getMusicItems(user));
      //  securityService.persistToken(userId, token);
		facebookService.register(accessToken, username, userId);
		return "redirect:/";
	}

	@RequestMapping(value = "/music", method = RequestMethod.GET)
	@ResponseBody
	public MusicFacebook getMusic(
			@CookieValue(value = "token", defaultValue = "") String token) {
		User user = SessionUtils.getUser(token);
		if (user == null) {
			throw new InternalServerErrorException();
		}

		return facebookService.getMusicItems(user);
	}

	// TODO remove session at logout
	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	public String logout(
			@CookieValue(value = "token", defaultValue = "") String token) {
		if (StringUtils.isEmpty(token)) {
			throw new BadRequestException();
		}

		SessionUtils.removeUserFromSession(token);
		return "redirect:/";
	}
}
