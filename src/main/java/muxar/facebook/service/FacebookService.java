package muxar.facebook.service;

import muxar.dto.User;
import muxar.gateway.domain.MusicFacebook;

/**
 * Created by Alice on 1/25/2016.
 */
public interface FacebookService {
    public MusicFacebook getMusicItems(User user);

    void register(String accessToken, String username, String userId);
}
