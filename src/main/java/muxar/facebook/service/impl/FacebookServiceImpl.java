package muxar.facebook.service.impl;

import muxar.dto.User;
import muxar.facebook.service.FacebookService;
import muxar.gateway.GatewayService;
import muxar.gateway.domain.MusicFacebook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.ws.rs.BadRequestException;

/**
 * Class intented to be used to make calls to FB API
 * Created by Alice on 1/25/2016.
 */
@Service
public class FacebookServiceImpl implements FacebookService{

    @Autowired
    private GatewayService gatewayService;

    public MusicFacebook getMusicItems(User user) {
        if(user == null || StringUtils.isEmpty(user.getUserId()) || StringUtils.isEmpty(user.getFbAccessToken())){
            throw new BadRequestException();
        }

        return gatewayService.getMusic(user.getUserId(), user.getFbAccessToken());
    }

    public void register(String accessToken, String username, String userId) {
        gatewayService.register(accessToken, username, userId);
    }
}
