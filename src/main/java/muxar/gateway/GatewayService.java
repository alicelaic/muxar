package muxar.gateway;

import muxar.dto.MusicItem;
import muxar.dto.User;
import muxar.gateway.domain.MusicFacebook;
import muxar.gateway.dto.MusicRecommendation;
import muxar.gateway.dto.PlayWrapper;
import muxar.playlist.domain.Playlist;

import java.util.List;

/**
 * Created by Alice on 1/25/2016.
 */
public interface GatewayService {

    /**
     * @param userId unique userId from facebook
     * @param accessToken access token generated to make FB Api call
     *
     */
    MusicFacebook getMusic(String userId, String accessToken);
    List<Playlist> getPlaylistsForUser(User user);
    /**
     * need token for authentication
     */
    List<Playlist> getPublicPlaylists(String token);

    void addSongToPlaylist(String playlistResource, String songResource, String songName, String artistName, String artistResource, String token);

    void register(String accessToken, String username, String userId);

    boolean validateToken(String token);

    void persistToken(String userId, String token);
	void addPlaylistToUser(User user, String playlistName, String playlistPrivacy, String token);
	void deletePlaylistForUser(User user, Playlist playlist);
    
    List<MusicItem> search(String token, String searchText);
	List<MusicItem> getSongsForPlaylist(String userToken, String playlistId);
    List<MusicItem> historyFor(User user);
    MusicRecommendation getRecommendation(User user);
    void play(PlayWrapper play);
    void downloadPlaylist(User user, String playlistResouce);

}
