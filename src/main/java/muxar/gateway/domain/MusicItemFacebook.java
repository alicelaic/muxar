package muxar.gateway.domain;

import java.io.Serializable;

/**
 * Created by Alice on 1/25/2016.
 */
public class MusicItemFacebook implements Serializable{
    private String name;
    private String id;
    private String created_time;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCreated_time() {
        return created_time;
    }

    public void setCreated_time(String created_time) {
        this.created_time = created_time;
    }
}
