package muxar.gateway.domain;

/**
 * Created by Alice on 1/25/2016.
 */
public class Paging {
    private Cursors cursors;

    public Cursors getCursors() {
        return cursors;
    }

    public void setCursors(Cursors cursors) {
        this.cursors = cursors;
    }
}
