package muxar.gateway.dto;

import muxar.dto.MusicItem;

import java.util.List;

/**
 * Created by Alice on 1/29/2016.
 */
public class MusicRecommendation {
	private List<MusicItem> songsOfArtist;
	private List<MusicItem> songsOfUser;
	private List<MusicItem> songsOfOtherUsers;
	private List<MusicItem> songsFromFacebook;

	public MusicRecommendation() {
	}

	public MusicRecommendation(List<MusicItem> songsOfArtist,
			List<MusicItem> songsOfUser, List<MusicItem> songsOfOtherUsers,
			List<MusicItem> songsFromFacebook) {
		this.songsOfArtist = songsOfArtist;
		this.songsOfUser = songsOfUser;
		this.songsOfOtherUsers = songsOfOtherUsers;
		this.songsFromFacebook = songsFromFacebook;
	}

	public List<MusicItem> getSongsOfArtist() {
		return songsOfArtist;
	}

	public void setSongsOfArtist(List<MusicItem> songsOfArtist) {
		this.songsOfArtist = songsOfArtist;
	}

	public List<MusicItem> getSongsOfUser() {
		return songsOfUser;
	}

	public void setSongsOfUser(List<MusicItem> songsOfUser) {
		this.songsOfUser = songsOfUser;
	}

	public List<MusicItem> getSongsOfOtherUsers() {
		return songsOfOtherUsers;
	}

	public void setSongsOfOtherUsers(List<MusicItem> songsOfOtherUsers) {
		this.songsOfOtherUsers = songsOfOtherUsers;
	}

	public List<MusicItem> getSongsFromFacebook() {
		return songsFromFacebook;
	}

	public void setSongsFromFacebook(List<MusicItem> songsFromFacebook) {
		this.songsFromFacebook = songsFromFacebook;
	}
	
}
