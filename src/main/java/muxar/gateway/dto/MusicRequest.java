package muxar.gateway.dto;

public class MusicRequest {

	private String token;
	private String searchText;

	public MusicRequest() {
	}

	public MusicRequest(String token, String searchText) {
		super();
		this.token = token;
		this.searchText = searchText;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getSearchText() {
		return searchText;
	}

	public void setSearchText(String searchText) {
		this.searchText = searchText;
	}

}
