package muxar.gateway.dto;

import java.util.List;

import muxar.dto.MusicItem;

public class MusicWrapper {
	
	private List<MusicItem> musicList;

	public List<MusicItem> getMusicList() {
		return musicList;
	}

	public void setMusicList(List<MusicItem> musicList) {
		this.musicList = musicList;
	}


}
