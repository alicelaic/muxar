package muxar.gateway.dto;

/**
 * Created by Alice on 1/29/2016.
 */
public class PlayWrapper {
    private String songResource;
    private String songName;
    private String artistResource;
    private String artistName;
    private String artistImage;
    private String token;
    private String userId;

    public PlayWrapper() {
    }

    public PlayWrapper(String songResource, String songName, String artistResource, String artistName, String artistImage, String token, String userId) {
        this.songResource = songResource;
        this.songName = songName;
        this.artistResource = artistResource;
        this.artistName = artistName;
        this.artistImage = artistImage;
        this.token = token;
        this.userId = userId;
    }

    public String getSongResource() {
        return songResource;
    }

    public void setSongResource(String songResource) {
        this.songResource = songResource;
    }

    public String getSongName() {
        return songName;
    }

    public void setSongName(String songName) {
        this.songName = songName;
    }

    public String getArtistResource() {
        return artistResource;
    }

    public void setArtistResource(String artistResource) {
        this.artistResource = artistResource;
    }

    public String getArtistName() {
        return artistName;
    }

    public void setArtistName(String artistName) {
        this.artistName = artistName;
    }

    public String getArtistImage() {
        return artistImage;
    }

    public void setArtistImage(String artistImage) {
        this.artistImage = artistImage;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
