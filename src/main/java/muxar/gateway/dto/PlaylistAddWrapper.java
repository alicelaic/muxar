package muxar.gateway.dto;

/**
 * Created by Alice on 1/29/2016.
 */
public class PlaylistAddWrapper {
    private String playlistName;
    private String playlistPrivacy;
    private String token;
    private String userId;

    public PlaylistAddWrapper(String playlistName, String playlistPrivacy, String token, String userId) {
        this.playlistName = playlistName;
        this.playlistPrivacy = playlistPrivacy;
        this.token = token;
        this.userId = userId;
    }

    public String getPlaylistName() {
        return playlistName;
    }

    public void setPlaylistName(String playlistName) {
        this.playlistName = playlistName;
    }

    public String getPlaylistPrivacy() {
        return playlistPrivacy;
    }

    public void setPlaylistPrivacy(String playlistPrivacy) {
        this.playlistPrivacy = playlistPrivacy;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
