package muxar.gateway.dto;

/**
 * Created by Alice on 1/29/2016.
 */
public class RecommendationRequest {
    private String token;
    private String userId;

    public RecommendationRequest() {
    }

    public RecommendationRequest(String token, String userId) {
        this.token = token;
        this.userId = userId;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
