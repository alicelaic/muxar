package muxar.gateway.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Alice on 1/29/2016.
 */
public class RegisterWrapper {

    @JsonProperty("access_token")
    private String token;
    private String username;
    private String userId;

    public RegisterWrapper() {
    }

    public RegisterWrapper(String token, String username, String userId) {
        this.token = token;
        this.username = username;
        this.userId = userId;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
