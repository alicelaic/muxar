package muxar.gateway.impl;

import muxar.ApplicationInit;
import muxar.dto.MusicItem;
import muxar.dto.User;
import muxar.gateway.GatewayService;
import muxar.gateway.domain.MusicFacebook;
import muxar.gateway.dto.*;
import muxar.playlist.domain.Playlist;
import muxar.playlist.domain.PlaylistRequest;
import muxar.utils.Constants;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.text.MessageFormat;
import java.util.Collections;
import java.util.List;

/**
 * Class used to make HTTP/HTTPS requests to external Created by Alice on
 * 1/25/2016.
 */
@Service
public class GatewayServiceImpl implements GatewayService{


    @Autowired
    private RestTemplate rt;

    public MusicFacebook getMusic(String userId, String accessToken) {
        String musicUrlFormatted = MessageFormat.format(Constants.URLS.MUSIC_FB_URL, userId, accessToken);
        ResponseEntity<MusicFacebook> responseEntity = rt.getForEntity(musicUrlFormatted, MusicFacebook.class);

        if(HttpStatus.OK.equals(responseEntity.getStatusCode())) {
            return responseEntity.getBody();
        }

        //return empty as not being any music preferences from user
        return new MusicFacebook();
    }

    public List<Playlist> getPlaylistsForUser(User user) {
        String url = ApplicationInit.applicationProperties.getProperty(Constants.Url_Keys.MUXAR_API_PLAYLIST_USER);
        url = MessageFormat.format(url, user.getFbAccessToken(), user.getUserId());
        ResponseEntity<PlaylistWrapper> responseEntity = rt.getForEntity(url, PlaylistWrapper.class);

        if(HttpStatus.OK.equals(responseEntity.getStatusCode())) {
            return responseEntity.getBody().getPlaylistList();
        }

        return Collections.EMPTY_LIST;
    }

    public List<Playlist> getPublicPlaylists(String token) {
        String url = ApplicationInit.applicationProperties.getProperty(Constants.Url_Keys.MUXAR_API_PUBLIC_PLAYLISTS);
        url = MessageFormat.format(url, token);
        ResponseEntity<PlaylistWrapper> responseEntity = rt.getForEntity(url, PlaylistWrapper.class);

        if(HttpStatus.OK.equals(responseEntity.getStatusCode())) {
            return responseEntity.getBody().getPlaylistList();
        }

        return Collections.EMPTY_LIST;
    }

    public void register(String accessToken, String username, String userId) {
        RegisterWrapper register = new RegisterWrapper(accessToken, username, userId);
        String url = ApplicationInit.applicationProperties.getProperty(Constants.Url_Keys.MUXAR_API_REGISTER);
        rt.postForEntity(url, register, Void.class);
  }

    public boolean validateToken(String token) {
        String url = ApplicationInit.applicationProperties.getProperty(Constants.Url_Keys.MUXAR_API_VALIDATE_TOKEN);
        ResponseEntity<Boolean> validateToken = rt.postForEntity(url, token, Boolean.class);
        return validateToken.getBody().booleanValue();
    }

    public void persistToken(String userId, String token) {
        TokenWrapper tokenWrapper = new TokenWrapper(userId, token);
        String url = ApplicationInit.applicationProperties.getProperty(Constants.Url_Keys.MUXAR_API_PERSIST_TOKEN);
        rt.postForEntity(url, tokenWrapper, Void.class);

    }
    
    public void addPlaylistToUser(User user, String playlistName, String playlistPrivacy, String token)	{
    	System.out.println("create playAddWrapper");
    	PlaylistAddWrapper playAddWrapper = new PlaylistAddWrapper(playlistName, playlistPrivacy, token, user.getUserId());
    	
    	String url = ApplicationInit.applicationProperties.getProperty(Constants.Url_Keys.MUXAR_API_ADD_PLAYLIST_TO_USER);
        System.out.println("created url = " + url);
    	rt.postForEntity(url, playAddWrapper, Void.class);
    }

	public void deletePlaylistForUser(User user, Playlist playlist) {
		// TODO Auto-generated method stub
		DeletePlaylistWrapper wrapper = new DeletePlaylistWrapper(user.getUserId(), user.getFbAccessToken(), 
				playlist.getId(), playlist.getName(), String.valueOf(playlist.isShared()));
		String url = ApplicationInit.applicationProperties.getProperty(Constants.Url_Keys.MUXAR_API_DELETE_PLAYLIST_FROM_USER);
        System.out.println("created url = " + url);
    	rt.postForEntity(url, wrapper, Void.class);
	}
	
	public List<MusicItem> search( String token, String searchText) {
		String url = ApplicationInit.applicationProperties
				.getProperty(Constants.Url_Keys.MUXAR_API_SEARCH);
		MusicRequest request = new MusicRequest(token, searchText);
		ResponseEntity<MusicWrapper> responseEntity = rt.postForEntity(url,
				request, MusicWrapper.class);

		if (HttpStatus.OK.equals(responseEntity.getStatusCode())) {
			return responseEntity.getBody().getMusicList();
		}

		return Collections.EMPTY_LIST;

	}

    public MusicRecommendation getRecommendation(User user) {
        String url = ApplicationInit.applicationProperties
                .getProperty(Constants.Url_Keys.MUXAR_API_RECOMMENDATION);
      //  RecommendationRequest request = new RecommendationRequest(token, userId);
        ResponseEntity<MusicRecommendation> responseEntity = rt.postForEntity(url,
                user, MusicRecommendation.class);

        if (HttpStatus.OK.equals(responseEntity.getStatusCode())) {
            return responseEntity.getBody();
        }

        return new MusicRecommendation();

    }
    
    public List<MusicItem> historyFor(User user) {
    	 String url = ApplicationInit.applicationProperties.getProperty(Constants.Url_Keys.MUXAR_API_HISTORY);
         url = MessageFormat.format(url, user.getFbAccessToken(), user.getUserId());
         ResponseEntity<MusicWrapper> responseEntity = rt.getForEntity(url, MusicWrapper.class);

         if(HttpStatus.OK.equals(responseEntity.getStatusCode())) {
             return responseEntity.getBody().getMusicList();
         }

         return Collections.EMPTY_LIST;
    }

    public void play(PlayWrapper play) {
        String url = ApplicationInit.applicationProperties
                .getProperty(Constants.Url_Keys.MUXAR_API_PLAY);
        rt.postForEntity(url, play, Void.class);
    }

    public void downloadPlaylist(User user, String playlistResouce) {
        DownloadRequest request = new DownloadRequest(user.getUserId(), user.getFbAccessToken(), playlistResouce);
        String url = ApplicationInit.applicationProperties
                .getProperty(Constants.Url_Keys.MUXAR_API_DOWNLOAD_PLAYLIST);
        rt.postForEntity(url, request, Void.class);
    }

	@Override
	public List<MusicItem> getSongsForPlaylist(String userToken, String playlistId) {
		System.out.println("gateway get songs for playlist");
		String url = ApplicationInit.applicationProperties.getProperty(Constants.Url_Keys.MUXAR_API_GET_SONGS_OF_PLAYLIST);
        url = MessageFormat.format(url, userToken, playlistId);
        ResponseEntity<MusicWrapper> responseEntity = rt.getForEntity(url, MusicWrapper.class);

        if(HttpStatus.OK.equals(responseEntity.getStatusCode())) {
            return responseEntity.getBody().getMusicList();
        }

        return Collections.EMPTY_LIST;
	}

	@Override
	public void addSongToPlaylist(String playlistResource, String songResource, String songName, String artistName,
			String artistResource, String token) {
		PlaylistRequest p = new PlaylistRequest(token, songResource, songName, artistName, artistResource, playlistResource);
        String url = ApplicationInit.applicationProperties.getProperty(Constants.Url_Keys.MUXAR_API_ADD_SONG_TO_PLAYLIST);
        rt.postForEntity(url, p, Void.class);
	}
}
