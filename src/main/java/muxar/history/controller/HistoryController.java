package muxar.history.controller;

import java.util.Map;

import muxar.gateway.GatewayService;
import muxar.utils.Constants;
import muxar.utils.session.SessionUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * @author alaic
 */
@Controller
@RequestMapping("/history")
public class HistoryController {

	@Autowired
	private GatewayService service;

	/**
	 * This method will get the history of the user
	 */
	@RequestMapping(method = RequestMethod.GET)
	public String getHistory(Map<String, Object> model,
			@CookieValue(value = "token", defaultValue = "") String token) {
		System.out.println("History");

		model.put(Constants.HISTORY_MODEL,
				service.historyFor(SessionUtils.getUser(token)));

		return "history";
	}

}
