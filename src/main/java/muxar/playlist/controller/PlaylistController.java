package muxar.playlist.controller;

import muxar.dto.MusicItem;
import muxar.dto.User;
import muxar.playlist.domain.Playlist;
import muxar.playlist.service.PlaylistService;
import muxar.utils.Constants;
import muxar.utils.Vocabulary;
import muxar.utils.session.SessionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.ws.rs.Consumes;
import javax.ws.rs.InternalServerErrorException;
import java.util.List;
import java.util.Map;

/**
 * Created by Alice on 1/3/2016.
 */
@Controller
public class PlaylistController {

    /**
     * using this, in playlistService will inject your implementation (e.g. PlaylistServiceImpl)
     */
    @Autowired
    private PlaylistService playlistService;
	/**
	 * This method will get all playlists of a user by his unique ID
	 * @param token
	 * @param model
	 * @return
	 */
    @RequestMapping(value = "/playlist", method = RequestMethod.GET)
    public String getAllPlaylists(@CookieValue(value = "token", defaultValue = "") String token,
    		Map<String, Object> model) {
    	
        System.out.println("Playlists @getPlaylists");
        
		User user = SessionUtils.getUser(token);
		if (user == null) {
			System.out.println("@getPlaylists: user is null");
			throw new InternalServerErrorException();
		}
		//System.out.println("@getPlaylists: user = " + user.getUserId());
		
		List<Playlist> playlists = playlistService.getPlaylistsForUser(user);

		model.put(Constants.ALL_USER_PLAYLISTS_MODEL, playlists);
		model.put("public" , "false");
		model.put("playlistsPage", true);
        return "playlist";
    }

    @RequestMapping(value="/playlist/all", method = RequestMethod.GET)
    @ResponseBody
    public List<Playlist> getPlaylists(@CookieValue(value = "token", defaultValue = "") String token) {
        System.out.println("all playlists");
        List<Playlist> playlists = playlistService.getPlaylistsForUser(SessionUtils.getUser(token));
        return playlists;
    }

    @RequestMapping(value="/playlist/song", method = RequestMethod.POST)
    @Consumes("application/json")
    @ResponseBody
    public void addSongToPlaylist(@RequestParam("songId")String songResource,
    		@RequestParam("playlistId")String playlistId,
    		@RequestParam("songName")String songName,
    		@RequestParam("artistName")String artistName,
    		@RequestParam("artistResource")String artistResource,
            @CookieValue(value = "token", defaultValue = "") String token) {
        playlistService.addSongToPlaylist(playlistId, songResource, songName, artistName, artistResource, token);
    }

	@RequestMapping(value="/playlist-public", method = RequestMethod.GET)
	@Consumes("application/json")
	public String getPublicPlaylists(@CookieValue(value = "token", defaultValue = "") String token, Model model) {
		List<Playlist> publicPlaylists = playlistService.getPublicPlaylists(token);
		model.addAttribute(Constants.ALL_USER_PLAYLISTS_MODEL, publicPlaylists);
		model.addAttribute("ppublic" , "true");

		return "playlist";
	}

	@RequestMapping(value = "/playlist/add", method = RequestMethod.POST)
	@Consumes("application/json")
	@ResponseBody
	public String addPlaylist(@RequestParam("playlistName") String playlistName,
							  @RequestParam("playlistPrivacy") String playlistPrivacy,
							  @CookieValue(value = "token", defaultValue = "") String token) {
		System.out.println("PlaylistController: @addPlaylist");

		System.out.println("new: " + SessionUtils.getUser(token).getUserId() + "  " + playlistName + "   "
				+ playlistPrivacy);

		playlistService.addPlaylistForUser(SessionUtils.getUser(token), playlistName, playlistPrivacy, token);

		return "success";
	}


	@RequestMapping(value = "/playlist/download", method = RequestMethod.POST)
	@Consumes("application/json")
	@ResponseBody
	public String addPlaylist(@RequestParam("playlistResource") String playlistResouce,
							  @CookieValue(value = "token", defaultValue = "") String token) {
		System.out.println("PlaylistController: @addPlaylist");


		playlistService.downloadPlaylist(SessionUtils.getUser(token), playlistResouce);

		return "success";
	}

    @RequestMapping(value = "/playlist/delete", method = RequestMethod.POST)
    @Consumes("application/json")
	@ResponseBody
	public String deletePlaylist(@RequestParam("playlistResource") String playlistResource,
			@RequestParam("playlistName") String playlistName,
			@RequestParam("playlistPrivacy") String playlistPrivacy,
			@CookieValue(value = "token", defaultValue = "") String token) {
    	System.out.println("PlaylistController: @deletePlaylist");   	
    	/*System.out.println("new: " + SessionUtils.getUser(token).getUserId() + "  " + playlistName + "   " 
    			+ playlistPrivacy+"="+playlist.isPrivat());*/
    	
    	Playlist playlist = new Playlist();
    	playlist.setId(playlistResource.replace(Vocabulary.MUXAR_URI, ""));
    	playlist.setShared(playlistPrivacy.equalsIgnoreCase("shared"));
    	playlist.setName(playlistName);
    	
    	playlistService.deletePlaylistForUser(SessionUtils.getUser(token), playlist);
    	
		return "success";
    }
    
    
    @RequestMapping(value="/playlist/song", method = RequestMethod.GET)
    @Consumes("application/json")
    @ResponseBody
    public List<MusicItem> getSongForPlaylist(@RequestParam("playlistId")String playlistId,
                                  @CookieValue(value = "token", defaultValue = "") String token) {
    	
    	System.out.println("get all song for playlist");
    	List<MusicItem> songs = playlistService.getSongForPlaylist(token, playlistId);   	
    	return songs;
    }
}
