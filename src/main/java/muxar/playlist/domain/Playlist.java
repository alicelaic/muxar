package muxar.playlist.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.hp.hpl.jena.graph.Node;
import com.hp.hpl.jena.graph.Triple;
import muxar.utils.Vocabulary;

/**
 * Created by Alice on 1/3/2016.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Playlist {
    private String name;
    private String id;
    private boolean shared;

    public Playlist() {
    }

    public Playlist(String name, String id) {
        this.name = name;
        this.id = id;

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

	public boolean isShared() {
		return shared;
	}

	public void setShared(boolean shared) {
		this.shared = shared;
	}
    
    @SuppressWarnings("deprecation")
	public Triple rdfIsPlaylist() {
        Node resource = Node.createURI(Vocabulary.MUXAR_URI + this.getId());
        Node type = Node.createURI(Vocabulary.RDF_URI + "type");
        Node playlistType = Node.createURI(Vocabulary.Type.PLAYLIST);
        
        /*System.out.println(Vocabulary.MUXAR_URI + this.getId() + " ----- " + Vocabulary.RDF_URI + "type" 
        					+ " ---- " +Vocabulary.Type.PLAYLIST);*/
        
		return new Triple(resource, type, playlistType);
	}
    
    @SuppressWarnings("deprecation")
	public Triple rdfPlaylistHasName() {
        Node resource = Node.createURI(Vocabulary.MUXAR_URI + this.getId());
        Node type = Node.createURI(Vocabulary.FOAF_URI + Vocabulary.Property.FOAF_NAME);
        Node playlistName = Node.createLiteral(this.name);

       /*System.out.println(Vocabulary.MUXAR_URI + this.getId() + " ----- " + Vocabulary.FOAF_URI + Vocabulary.Property.FOAF_NAME 
        		+ " ---- " + this.getName());*/
        
		return new Triple(resource, type, playlistName);
	}
    
    @SuppressWarnings("deprecation")
	public Triple rdfPlaylistHasPrivacy() {
    	String privat = "shared";
    	if (this.isShared() == false) {
    		privat = "private";
    	}
    	
		Node resource = Node.createURI(Vocabulary.MUXAR_URI + this.getId());
        Node type = Node.createURI(Vocabulary.SCHEMA_URI + Vocabulary.Property.SCHEMA_ACCESIBILITY);
        Node privacy = Node.createLiteral(privat);
        
        System.out.println(Vocabulary.MUXAR_URI + this.getId() + " ----- " + Vocabulary.SCHEMA_URI 
        		+ Vocabulary.Property.SCHEMA_ACCESIBILITY + " ---- " + privat);
        
		return new Triple(resource, type, privacy);
    }
}
