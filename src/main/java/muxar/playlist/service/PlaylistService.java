package muxar.playlist.service;

import muxar.dto.MusicItem;
import muxar.dto.User;
import muxar.playlist.domain.Playlist;

import java.util.List;

/**
 * Created by Alice on 1/3/2016.
 */
public interface PlaylistService {
    void addSongToPlaylist(String playlistId, String songId, String songName, String artistName, String artistResource, String token);
    List<Playlist> getPlaylistsForUser(User user);
    List<Playlist> getPublicPlaylists(String token);
    void addPlaylistForUser(User user, String playlistName, String playlistPrivacy, String token);
    void deletePlaylistForUser(User user, Playlist playlist);
	List<MusicItem> getSongForPlaylist(String token, String playlistId);
    void downloadPlaylist(User user, String playlistResouce);
}
