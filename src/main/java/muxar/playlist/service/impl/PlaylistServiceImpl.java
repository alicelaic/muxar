package muxar.playlist.service.impl;

import muxar.dto.MusicItem;
import muxar.dto.User;
import muxar.gateway.GatewayService;
import muxar.playlist.domain.Playlist;
import muxar.playlist.service.PlaylistService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Alice on 1/3/2016.
 */
@Service
public class PlaylistServiceImpl implements PlaylistService{

    @Autowired
    private GatewayService gatewayService;

    public void addSongToPlaylist(String playlistResource, String songResource, String songName, String artistName,
			String artistResource, String token) {
        gatewayService.addSongToPlaylist(playlistResource, songResource, songName, artistName, artistResource, token);
    }

    /**
     * returns the playlists for an user
     * @param user specific user logged in
     */
    public List<Playlist> getPlaylistsForUser(User user) {
        List<Playlist> playlists = gatewayService.getPlaylistsForUser(user);
        return playlists;
    }


    /**
     *Return public playlists
    */

    public List<Playlist> getPublicPlaylists(String token) {
       return gatewayService.getPublicPlaylists(token);
    }

	public void addPlaylistForUser(User user, String playlistName, String playlistPrivacy, String token) {
		gatewayService.addPlaylistToUser(user, playlistName, playlistPrivacy, token);
	}

	public void deletePlaylistForUser(User user, Playlist playlist) {
		gatewayService.deletePlaylistForUser(user, playlist);
	}

	@Override
	public List<MusicItem> getSongForPlaylist(String userToken, String playlistId) {
		// TODO Auto-generated method stub
		System.out.println("playlistService get songs for playlist");
		return gatewayService.getSongsForPlaylist(userToken, playlistId);
	}
		
    public void downloadPlaylist(User user, String playlistResouce) {
        gatewayService.downloadPlaylist(user, playlistResouce);
    }


}
