package muxar.security;

/**
 * Created by Alice on 1/25/2016.
 */
public interface SecurityService {
    void persistToken(String userId, String token);
    boolean validateToken(String token);
}
