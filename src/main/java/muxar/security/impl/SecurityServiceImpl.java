package muxar.security.impl;

import muxar.gateway.GatewayService;
import muxar.security.SecurityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Alice on 1/25/2016.
 */
@Service
public class SecurityServiceImpl implements SecurityService {

    @Autowired
    private GatewayService gatewayService;

    public void persistToken(String userId, String token) {
       gatewayService.persistToken(userId, token);
    }

    public boolean validateToken(String token) {
     return gatewayService.validateToken(token);
    }

}
