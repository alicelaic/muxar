package muxar.service;

import muxar.dto.MusicArtist;
import muxar.dto.MusicItem;
import muxar.utils.Vocabulary;

import com.hp.hpl.jena.query.ParameterizedSparqlString;
import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QuerySolution;

public class DbpediaQueryBuilder {

	public static final String SONG_RES = "song";
	public static final String ARTIST_RES = "artist";

	private static final String GET_BY_ARTIST_SQL = "SELECT ?"
			+ SONG_RES
			+ " ?l WHERE {"
			+ " ?song rdfs:label ?label.\n ?song dbo:thumbnail ?thumbnail.\n ?"
			+ SONG_RES
			+ " dbo:musicalArtist ?"
			+ ARTIST_RES
			+ ". FILTER (lang(?label) = 'en') \n BIND( str(?label) as ?l)} LIMIT 3";

	private static final String GET_SONGS_SELECT = "SELECT DISTINCT ?song ?sTitle ?artist ?sArtistName ?img";

	public static Query getSongsByArtist(MusicArtist artist) {

		ParameterizedSparqlString qs = new ParameterizedSparqlString();

		qs.setNsPrefix(Vocabulary.DBPedia.ONTOLOGY_PREFIX,
				Vocabulary.DBPedia.ONTOLOGY_URI);
		qs.setNsPrefix(Vocabulary.DBPedia.RESOURCE_PREFIX,
				Vocabulary.DBPedia.RESOURCE_URI);
		qs.setNsPrefix(Vocabulary.RDFS_PREFIX, Vocabulary.RDFS_URI);

		qs.append(GET_BY_ARTIST_SQL);
		qs.setIri(ARTIST_RES,
				Vocabulary.DBPedia.RESOURCE_URI + artist.getName());

		return qs.asQuery();

	}

	public static Query getSongsByArtist(String name) {

		ParameterizedSparqlString qs = new ParameterizedSparqlString();

		qs.setNsPrefix(Vocabulary.DBPedia.ONTOLOGY_PREFIX,
				Vocabulary.DBPedia.ONTOLOGY_URI);
		qs.setNsPrefix(Vocabulary.DBPedia.RESOURCE_PREFIX,
				Vocabulary.DBPedia.RESOURCE_URI);
		qs.setNsPrefix(Vocabulary.RDFS_PREFIX, Vocabulary.RDFS_URI);
		qs.setNsPrefix(Vocabulary.FOAF_PREFIX, Vocabulary.FOAF_URI);

		qs.append(GET_SONGS_SELECT);
		qs.append(" WHERE {\n ");
		qs.append("?song dbo:musicalArtist ?artist. \n ");
		qs.append("?song foaf:name ?songName. \n");
		qs.append("?artist foaf:name ?artistName. \n");
		qs.append("?artist dbo:thumbnail ?img. \n");
		qs.append("FILTER (lcase(str(?artistName))= ?artistParam) \n");
		qs.append("FILTER(lang(?artistName)='en') \n");
		qs.append("BIND( str(?songName) as ?sTitle) \n");
		qs.append("BIND( str(?artistName) as ?sArtistName) } LIMIT 5");

		qs.setLiteral("artistParam", name.toLowerCase());

		return qs.asQuery();

	}

	public static Query getSongsByGenre(String genre) {

		ParameterizedSparqlString qs = new ParameterizedSparqlString();

		qs.setNsPrefix(Vocabulary.DBPedia.ONTOLOGY_PREFIX,
				Vocabulary.DBPedia.ONTOLOGY_URI);
		qs.setNsPrefix(Vocabulary.DBPedia.RESOURCE_PREFIX,
				Vocabulary.DBPedia.RESOURCE_URI);
		qs.setNsPrefix(Vocabulary.RDFS_PREFIX, Vocabulary.RDFS_URI);
		qs.setNsPrefix(Vocabulary.FOAF_PREFIX, Vocabulary.FOAF_URI);

		qs.append(GET_SONGS_SELECT);
		qs.append(" WHERE {\n ");
		qs.append("?song dbo:musicalArtist ?artist. \n ");
		qs.append("?song foaf:name ?songName. \n");
		qs.append("?artist foaf:name ?artistName. \n");
		qs.append("?artist dbo:thumbnail ?img. \n");
		qs.append("?song dbo:genre ?genre. \n");
		qs.append("?genre rdfs:label ?genreLabel. \n");
		qs.append("FILTER contains(?genreLabel, '" + genre + "') \n");
		qs.append("FILTER(lang(?genreLabel)='en') \n");
		qs.append("BIND( str(?songName) as ?sTitle) \n");
		qs.append("BIND( str(?artistName) as ?sArtistName) } LIMIT 50");

		return qs.asQuery();

	}

	public static Query getSongsBy(String name) {

		ParameterizedSparqlString qs = new ParameterizedSparqlString();

		qs.setNsPrefix(Vocabulary.RDF_PREFIX, Vocabulary.RDF_URI);
		qs.setNsPrefix(Vocabulary.DBPedia.ONTOLOGY_PREFIX,
				Vocabulary.DBPedia.ONTOLOGY_URI);
		qs.setNsPrefix(Vocabulary.FOAF_PREFIX, Vocabulary.FOAF_URI);

		qs.append(GET_SONGS_SELECT);
		qs.append(" WHERE {\n ");
		qs.append("?song rdf:type dbo:Single . \n ");
		qs.append("?song dbo:musicalArtist ?artist. \n");
		qs.append("?artist dbo:thumbnail ?img. \n");
		qs.append("?artist foaf:name ?artistName. \n ");
		qs.append("?song foaf:name ?title. \n");
		qs.append("FILTER regex(lcase(str(?title)), ?titleParam) \n");
		qs.append("FILTER(lang(?title)='en') \n");
		qs.append("BIND( str(?artistName) as ?sArtistName) \n");
		qs.append("BIND( str(?title) as ?sTitle) } LIMIT 5");

		qs.setLiteral("titleParam", name.toLowerCase());

		return qs.asQuery();

	}
	
	public static MusicItem from(QuerySolution solution)
	{
		MusicItem song = new MusicItem();
		MusicArtist artist = new MusicArtist(solution.getLiteral("?sArtistName")
				.toString());
		artist.setResourceName(solution.getResource("artist").toString());
		artist.setImage(solution.getResource("img").toString());
		song.setArtist(artist);
		
		song.setName(solution.getLiteral("?sTitle").toString());
		song.setResourceName(solution.getResource("song").toString());
		
		return song;
	}
}
