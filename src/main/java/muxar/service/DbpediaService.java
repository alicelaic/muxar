package muxar.service;

import java.util.ArrayList;
import java.util.List;

import muxar.ApplicationInit;
import muxar.dto.MusicArtist;
import muxar.dto.MusicItem;
import muxar.utils.Constants;
import muxar.utils.Vocabulary;
import virtuoso.jena.driver.VirtGraph;

import com.hp.hpl.jena.graph.Node;
import com.hp.hpl.jena.graph.Triple;
import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;

public class DbpediaService {

	public static List<MusicItem> getSongsOf(String artist) {

		List<MusicItem> songs = new ArrayList<MusicItem>();
		Query query = DbpediaQueryBuilder.getSongsByArtist(artist);
		QueryExecution qe = QueryExecutionFactory.sparqlService(
				Vocabulary.DBPedia.SPARQL_ENDPOINT, query);

		ResultSet result = qe.execSelect();
		while (result.hasNext()) {
			QuerySolution solution = result.next();
			if (solution != null) {
				songs.add(DbpediaQueryBuilder.from(solution));
			}
		}

		return songs;
	}
	
	public static List<MusicItem> getSongsOfArtist(String name) {

		List<MusicItem> songs = new ArrayList<MusicItem>();
		Query query = DbpediaQueryBuilder.getSongsByArtist(name);
		QueryExecution qe = QueryExecutionFactory.sparqlService(
				Vocabulary.DBPedia.SPARQL_ENDPOINT, query);

		ResultSet result = qe.execSelect();
		while (result.hasNext()) {
			QuerySolution solution = result.next();
			if (solution != null) {
				songs.add(DbpediaQueryBuilder.from(solution));
			}
		}

		return songs;
	}

	public static List<MusicItem> getSongsByGenre(String genre) {
		List<MusicItem> songs = new ArrayList<MusicItem>();
		Query query = DbpediaQueryBuilder.getSongsByGenre(genre);
		QueryExecution qe = QueryExecutionFactory.sparqlService(
				Vocabulary.DBPedia.SPARQL_ENDPOINT, query);

		ResultSet result = qe.execSelect();
		while (result.hasNext()) {
			QuerySolution solution = result.next();
			if (solution != null) {
				songs.add(DbpediaQueryBuilder.from(solution));
			}
		}

		return songs;
	}
	
	public static List<MusicItem> getSongsBy(String name) {
		List<MusicItem> songs = new ArrayList<MusicItem>();
		Query query = DbpediaQueryBuilder.getSongsBy(name);
		QueryExecution qe = QueryExecutionFactory.sparqlService(
				Vocabulary.DBPedia.SPARQL_ENDPOINT, query);

		ResultSet result = qe.execSelect();
		while (result.hasNext()) {
			QuerySolution solution = result.next();
			if (solution != null) {
				songs.add(DbpediaQueryBuilder.from(solution));
			}
		}

		return songs;
	}
	
    public static boolean userExists(String username, String userId) {
		String url = ApplicationInit.getProperty(Constants.VIRT_URL);
		String usernameVirt = ApplicationInit.getProperty(Constants.VIRT_USERNAME);
		String passwordVirt = ApplicationInit.getProperty(Constants.VIRT_PASSWORD);
		VirtGraph graph = new VirtGraph ("MUXAR", url, usernameVirt, passwordVirt);

		Node s = Node.createURI(Vocabulary.MUXAR_URI + userId);
		Node p = Node.createURI(Vocabulary.RDF_URI);
		Node o = Node.createURI(Vocabulary.MUXAR_URI + "User");

		return graph.contains(new Triple(s,p,o));
	}


	public static void persistUser(String username, String userId) {
		if(!userExists(username, userId)){
			createUser(username, userId);
		}
	}

	private static void createUser(String username, String userId) {
		String url = ApplicationInit.getProperty(Constants.VIRT_URL);
		String usernameVirt = ApplicationInit.getProperty(Constants.VIRT_USERNAME);
		String passwordVirt = ApplicationInit.getProperty(Constants.VIRT_PASSWORD);
		String graphName = ApplicationInit.getProperty(Constants.VIRT_GRAPH);
		VirtGraph graph = new VirtGraph (graphName, url, usernameVirt, passwordVirt);

		Node s = Node.createURI(Vocabulary.MUXAR_URI + userId);
		Node p = Node.createURI(Vocabulary.RDF_URI);
		Node o = Node.createURI(Vocabulary.MUXAR_URI + "User");

		graph.add(new Triple(s,p,o));

	}

}