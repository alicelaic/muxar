package muxar.service;

import java.util.ArrayList;
import java.util.List;

import virtuoso.jena.driver.VirtuosoQueryExecution;
import virtuoso.jena.driver.VirtuosoQueryExecutionFactory;

import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;

import muxar.dto.MusicArtist;
import muxar.dto.MusicItem;
import muxar.dto.User;
import muxar.virtuoso.Graph;

public class MuxarService {

	public static List<MusicItem> getSongsListenedByOtherUsersFor(User user) {
		List<MusicItem> songs = new ArrayList<MusicItem>();
		Query query = MuxarQueryBuilder.getSongsListenedByOtherUserFor(user);
		VirtuosoQueryExecution qe = VirtuosoQueryExecutionFactory.create(query,
				new Graph().connection());

		ResultSet result = qe.execSelect();
		while (result.hasNext()) {
			QuerySolution solution = result.next();
			if (solution != null) {

				MusicItem song = new MusicItem();
				MusicArtist artist = new MusicArtist(solution.getLiteral(
						"?artistName").toString());
				artist.setResourceName(solution.getResource("artist")
						.toString());
				artist.setImage(solution.getResource("img").toString());
				song.setArtist(artist);

				song.setName(solution.getLiteral("songName").toString());
				song.setResourceName(solution.getResource("song").toString());
				songs.add(song);
				System.out.println(song.getName());
			}
		}

		return songs;

	}

	public static String getArtistListenedBy(User user) {
		Query query = MuxarQueryBuilder.getArtistListenedBy(user);
		VirtuosoQueryExecution qe = VirtuosoQueryExecutionFactory.create(query,
				new Graph().connection());

		ResultSet result = qe.execSelect();
		while (result.hasNext()) {
			QuerySolution solution = result.next();
			if (solution != null) {

				return solution.getLiteral("artistName").toString();
			}
		}

		return "";

	}

	public static List<MusicItem> getSongsListenedBy(User user) {
		List<MusicItem> songs = new ArrayList<MusicItem>();
		Query query = MuxarQueryBuilder.getSongsListenedBy(user);
		VirtuosoQueryExecution qe = VirtuosoQueryExecutionFactory.create(query,
				new Graph().connection());

		ResultSet result = qe.execSelect();
		while (result.hasNext()) {
			QuerySolution solution = result.next();
			if (solution != null) {

				MusicItem song = new MusicItem();
				MusicArtist artist = new MusicArtist(solution.getLiteral(
						"?artistName").toString());
				artist.setResourceName(solution.getResource("artist")
						.toString());
				artist.setImage(solution.getResource("img").toString());
				song.setArtist(artist);

				song.setName(solution.getLiteral("songName").toString());
				song.setResourceName(solution.getResource("song").toString());
				songs.add(song);
				System.out.println(song.getName());
			}
		}

		return songs;

	}

}
