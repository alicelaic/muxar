package muxar.utils;

public class Constants {

	public static final String RECOMMENDATION_MODEL = "recommender";
	public static final String ALL_USER_PLAYLISTS_MODEL = "userPlaylists";
	public static final String HISTORY_MODEL = "userHistory";

	public static final String VIRT_URL = "virtuoso.url";
	public static final String VIRT_USERNAME = "virtuoso.username";
	public static final String VIRT_PASSWORD = "virtuoso.password";
	public static final String VIRT_GRAPH = "virtuoso.graph.name";

	public class URLS {
		public static final String MUSIC_FB_URL = "https://graph.facebook.com/v2.5/{0}/music?access_token={1}";

	}

	public class Url_Keys {
		public static final String MUXAR_API_PLAYLIST_USER = "url.api.playlist.user";
		public static final String MUXAR_API_PUBLIC_PLAYLISTS = "url.api.public.playlists";
		public static final String MUXAR_API_ADD_SONG_TO_PLAYLIST = "url.api.add.song.playlist";
		public static final String MUXAR_API_REGISTER = "url.api.register";
		public static final String MUXAR_API_PERSIST_TOKEN = "url.api.persist.token";
		public static final String MUXAR_API_VALIDATE_TOKEN = "url.api.validate.token";
		public static final String MUXAR_API_ADD_PLAYLIST_TO_USER = "url.api.add.playlist.user";
		public static final String MUXAR_API_DELETE_PLAYLIST_FROM_USER = "url.api.delete.playlist.user";
		public static final String MUXAR_API_SEARCH = "url.api.search";
		public static final String MUXAR_API_RECOMMENDATION = "url.api.recommendation";
		public static final String MUXAR_API_GET_SONGS_OF_PLAYLIST = "url.api.get.song.playlist";
		public static final String MUXAR_API_PLAY = "url.api.play";
		public static final String MUXAR_API_DOWNLOAD_PLAYLIST = "url.api.download";
		public static final String MUXAR_API_HISTORY = "url.api.history";
	}

	public static final String TOKEN = "token";
	public static final String SEARCH_BY_ARTIST_TAG = "artist:";
	public static final String SEARCH_BY_GENRE_TAG = "genre:";
	public static final String MUSIC_FROM_SEARCH = "isSearch";

}
