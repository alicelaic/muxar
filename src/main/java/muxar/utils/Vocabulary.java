package muxar.utils;

public class Vocabulary {

	public static final String MUXAR_URI = "http://muxar.net/";
	public static final String MUXAR_PREFIX = "mx";
	public static final String RDFS_URI = "http://www.w3.org/2000/01/rdf-schema#";
	public static final String RDFS_PREFIX = "rdfs";
	public static final String RDF_URI = "http://www.w3.org/1999/02/22-rdf-syntax-ns#";
	public static final String RDF_PREFIX = "rdf";
	public static final String FOAF_PREFIX = "foaf";
	public static final String FOAF_URI = "http://xmlns.com/foaf/0.1/";
	public static final String SCHEMA_URI = "http://schema.org/";
	public static final String SCHEMA_PREFIX = "schema";
	public static final String MUXAR_HAS_TOKEN = "http://muxar.net/hasToken";

	public class Property {
		public static final String LISTEN = "listen";
		public static final String FOAF_KNOWS = "knows";
		public static final String SCHEMA_IN_PLAYLIST = "inPlaylist";
		public static final String FOAF_NAME = "name";
		public static final String RDF_TYPE = "type";
		public static final String SCHEMA_IMAGE = "image";
		public static final String SCHEMA_ARTIST = "byArtist";
		public static final String SCHEMA_HAS_PLAYLIST = "hasPlaylist";
		public static final String SCHEMA_ACCESIBILITY = "accessibilityControl";
	}

	public class DBPedia {
		public static final String SPARQL_ENDPOINT = "http://dbpedia.org/sparql";
		public static final String ONTOLOGY_URI = "http://dbpedia.org/ontology/";
		public static final String RESOURCE_URI = "http://dbpedia.org/resource/";

		public static final String RESOURCE_PREFIX = "dbr";
		public static final String ONTOLOGY_PREFIX = "dbo";

	}

	public class Type {
		public static final String ARTIST = SCHEMA_URI + "MusicGroup";
		public static final String SONG = SCHEMA_URI + "MusicRecording";
		public static final String PLAYLIST = SCHEMA_URI + "MusicPlaylist";
	}

}
