package muxar.utils.session;

import muxar.dto.User;
import muxar.gateway.domain.MusicFacebook;

import org.springframework.util.StringUtils;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpSession;

/**
 * Created by Alice on 1/25/2016.
 */
public class SessionUtils {
    public static HttpSession session() {
        ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
        return attr.getRequest().getSession(true); // true == allow create
    }

    public static User getUser(String token) {
        if(StringUtils.isEmpty(token)) {
            return null;
        }

        return (User)session().getAttribute(token);
    }

    public static void setUser(String userId, String name, String fbToken,MusicFacebook musicFacebook) {
    	
        User user = new User(userId, name, fbToken);
        user.setMusicFacebook(musicFacebook);
        session().setAttribute(fbToken, user);
    }

    public static void removeUserFromSession(String token) {
        session().removeAttribute(token);
    }
}
