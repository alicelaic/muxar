package muxar.virtuoso;

import muxar.ApplicationInit;
import muxar.utils.Constants;
import virtuoso.jena.driver.VirtGraph;

public class Graph {

	public VirtGraph connection() {

		String url = ApplicationInit.getProperty(Constants.VIRT_URL);
		String usernameVirt = ApplicationInit
				.getProperty(Constants.VIRT_USERNAME);
		String passwordVirt = ApplicationInit
				.getProperty(Constants.VIRT_PASSWORD);
		String graphName = ApplicationInit.getProperty(Constants.VIRT_GRAPH);

		return new VirtGraph(graphName, url, usernameVirt, passwordVirt);
	}

}
