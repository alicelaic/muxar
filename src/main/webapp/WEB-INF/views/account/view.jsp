<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script type="text/javascript" src="//code.jquery.com/jquery-1.9.1.js"></script>
<script
	src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.js"
	type="text/javascript"></script>
<script type="text/javascript"
	src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script>

<input type="button" id="lire" value="Lire Values" />
<input type="text" id="lireValue" />
<input type="button" id="downloadImages" value="Download" />
<input type="url" id="imageToFound" />
<input type="button" id="imageToFoundBtn" value="Proceed" />
<div id="request"></div>
<table>
	<c:forEach items="${images}" var="image">
		<tr>

			<td>
				<h1>${image.name }</h1> <c:set var="tagsName" value="" /> <c:forEach
					items="${image.tagsSelected}" var="tag">
					<c:set var="tagsName" value="${tag.tag.name},, ${tagsName }" />
				</c:forEach> <a data-id="${tagsName }" id="${image.id }"><img
					src="${image.link }" /></a> <c:forEach items="${image.tagsSelected}"
					var="tag">
					<span>${tag.tag.name}</span>
				</c:forEach>
			</td>
		</tr>
	</c:forEach>
</table>
<script>
	$("#imageToFoundBtn").on("click", function() {
		var valueUrl = $("#imageToFound").val();
		$.ajax({
			type : 'POST',
			url : 'searchImage',
			data : {
				url : valueUrl
			},
			success : function(data) {
				$("#request").html(data);
			}
		});

	});

	$("#lire").on("click", function() {
		var title = $("#lireValue").val();
		$.ajax({
			type : 'GET',
			url : 'lire',
			data : {
				title : title
			},
			success : function(data) {
				$("#request").html(data);

			}
		});
	});
	$("#downloadImages").on("click", function() {
		$.ajax({
			type : 'GET',
			url : 'downloadAllImages',
			success : function(data) {
			}
		});
	});

	$("a")
			.on(
					"click",
					function() {
						var idImage = this.id;
						var keys = $(this).data('id');
						var arrayTag = keys.split(",");
						var query = arrayTag[0];
						for (var i = 1; i < arrayTag.length - 1; i++) {
							query = query + "%20" + arrayTag[i];
						}
						var url = "https://ajax.googleapis.com/ajax/services/search/images?v=1.0&q="
								+ query;

						$.ajax({
									url : url,
									dataType : 'jsonp',
									success : function(data) {
										var jsonResponse = data;
										var resultsNo = jsonResponse.responseData.results.length;
										var links = jsonResponse.responseData.results[0].unescapedUrl
												+ ";;"
												+ jsonResponse.responseData.results[0].contentNoFormatting;
										for (var i = 1; i < resultsNo; i++) {
											links = links
													+ "@_@"
													+ jsonResponse.responseData.results[i].unescapedUrl
													+ ";;"
													+ jsonResponse.responseData.results[i].contentNoFormatting;
										}
										$.ajax({
											type : 'POST',
											url : 'links',
											data : {
												'links' : links,
												'idImage' : idImage
											},
											success : function(data) {
											}
										});
									}
								});

					});
</script>
