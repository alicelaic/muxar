  <!-- Modal edit playlist-->
  <div class="modal fade" id="myModalEdit" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Tracks</h4>
        </div>
        <div class="modal-body">
        
          <ul class="list-group">
  			<li class="list-group-item"><span class="badge">12</span> New</li>
  		  </ul>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
        </div>
      </div>
   </div>
 </div>