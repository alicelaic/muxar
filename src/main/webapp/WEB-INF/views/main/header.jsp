<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<ul class="nav nav-pills">
    <li role="presentation" tab-value="home"><a href="home"><spring:message code="message.home" /></a></li>
    <li role="presentation" tab-value="playlist"><a href="playlist"><spring:message code="message.myPlaylists" /></a></li>
    <li role="presentation" tab-value="history"><a href="history"><spring:message code="message.history" /></a></li>
    <li role="presentation" tab-value="playlist-public"><a href="playlist-public"><spring:message code="message.publicPlaylists" /></a></li>
    <li role="presentation" tab-value="history" ><div class="fb-login-button" data-max-rows="1" data-size="large" data-show-faces="false" data-auto-logout-link="true" data-scope="public_profile,email,user_events, user_actions.music, user_likes" onlogin="checkLoginState();"></div>
    </li>
</ul>

<script type="text/javascript" src="${pageContext.servletContext.contextPath}/resources/js/jquery-1.11.3.js"></script>
<script type="text/javascript" src="${pageContext.servletContext.contextPath}/resources/js/header.js"></script>
<script type="text/javascript" src="${pageContext.servletContext.contextPath}/resources/js/facebook/login.js"></script>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


