<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:forEach items="${userHistory}" var="song">
	<div class="panel panel-default">
		<div class="panel-heading">
		</div>
		<div class="panel-body">
			<div class="left-panel">
				<span class="photo-song"><img width="65" height="65"
					src="${song.artist.image}" /> </span>
			</div>

			<div class="right-panel">
				<div class="song">
					<a href="${song.url}" target="_blank">${song.name}</a>
				</div>
				<div class="artist" dbpedia-owl="${song.artist.resourceName}">${song.artist.name}</div>
			</div>
		</div>

		<div class="panel-footer custom-footer" id="">
			<span class="glyphicon glyphicon-play-circle" id="playBtn"
				data-song-res="${song.resourceName}" data-song-name="${song.name}"
				data-song-artist-name="${song.artist.name}"
				data-song-artist-img="${song.artist.image}"
				data-song-artist-res="${song.artist.resourceName}"
				data-song-url="${song.url}"> </span>
		</div>
	</div>
</c:forEach>

<script>
	$(document).ready(function() {

		$(document).on('click', '#playBtn', function() {

			var songResource = $(this).attr('data-song-res');
			var songName = $(this).attr('data-song-name');
			var artistName = $(this).attr('data-song-artist-name');
			var artistImg = $(this).attr('data-song-artist-img');
			var artistResource = $(this).attr('data-song-artist-res');
			var url = $(this).attr('data-song-url');

			$.ajax({
				type : 'POST',
				url : '${pageContext.request.contextPath}/play',
				data : {
					songResource : songResource,
					songName : songName,
					artistResource : artistResource,
					artistName : artistName,
					artistImage : artistImg
				},
				success : function(data) {
					window.open(url);
				},
				error : function(data) {

				}
			});
		});
	});
</script>