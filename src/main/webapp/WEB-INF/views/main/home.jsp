<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<jsp:include page="search.jsp"></jsp:include>
</br>

<c:forEach items="${recommender.songsOfArtist}" var="song">
	<div class="panel panel-default">
		<div class="panel-heading">
			<c:if test="${not isSearch}">
				<spring:message code="message.anotherSongFrom" /> ${song.artist.name}</c:if>
		</div>
		<div class="panel-body">
			<div class="left-panel">
				<span class="photo-song"><img width="65" height="65" itemprop="thumbnail"
					src="${song.artist.image}" /> </span>
			</div>

			<div class="right-panel" itemprop="track" itemscope itemtype="http://schema.org/MusicRecording">
				<div class="song" itemprop="name">
					<a itemprop="url" target="_blank">${song.name}</a>
				</div>
				<div class="artist" itemprop="byArtist" dbpedia-owl="${song.artist.resourceName}">${song.artist.name}</div>
			</div>
		</div>

		<div class="panel-footer custom-footer">
			<button type="button" class="btn btn-default btn-sm"  id="playBtn"
				data-song-res="${song.resourceName}" data-song-name="${song.name}"
				data-song-artist-name="${song.artist.name}"
				data-song-artist-img="${song.artist.image}"
				data-song-artist-res="${song.artist.resourceName}"
				data-song-url="${song.url}">
				<span class="glyphicon glyphicon-play-circle"> </span>
			</button>
			<div class="addToPlaylist">
				<button type="button" class="btn btn-default btn-sm"
				 data-toggle="popover"
						title="Add to playlist..." data-html="true" data-placement="bottom"
						onclick="addToPlaylist(this)" 
						dbpedia-owl="${song.resourceName}"
						data-song-name="${song.name}"
						data-song-artist-name="${song.artist.name}"
						data-song-artist-res="${song.artist.resourceName}">
					<span class="glyphicon glyphicon-plus-sign"></span>
				</button>
			</div>
		</div>
	</div>
</c:forEach>

<c:forEach items="${recommender.songsOfOtherUsers}" var="song">
	<div class="panel panel-default">
		<div class="panel-heading">
			<c:if test="${not isSearch}">
				<spring:message code="message.anotherSongMuxar" />
			</c:if>
		</div>

		<div class="panel-body">
			<div class="left-panel">
				<span class="photo-song"><img width="65" height="65" itemprop="thumbnail"
					src="${song.artist.image}" /> </span>
			</div>

			<div class="right-panel" itemscope itemtype="http://schema.org/MusicRecording">
				<div class="song" itemprop="name">
					<a itemprop="url" target="_blank">${song.name}</a>
				</div>
				<div class="artist" dbpedia-owl="${song.artist.resourceName}">${song.artist.name}</div>
			</div>
		</div>

		<div class="panel-footer custom-footer">
			<button type="button" class="btn btn-default btn-sm" id="playBtn"
					data-song-res="${song.resourceName}" data-song-name="${song.name}"
					data-song-artist-name="${song.artist.name}"
					data-song-artist-img="${song.artist.image}"
					data-song-artist-res="${song.artist.resourceName}">
				<span class="glyphicon glyphicon-play-circle"> </span>
			</button>
			<div class="addToPlaylist">
				<button type="button" class="btn btn-default btn-sm" data-toggle="popover"
						title="Add to playlist..." data-html="true"
						data-placement="bottom" onclick="addToPlaylist(this)"
						dbpedia-owl="${song.resourceName}"
						data-song-name="${song.name}"
						data-song-artist-name="${song.artist.name}"
						data-song-artist-res="${song.artist.resourceName}">
					<span class="glyphicon glyphicon-plus-sign">
					</span>
				</button>
			</div>
		</div>
	</div>
</c:forEach>

<c:forEach items="${recommender.songsOfUser}" var="song">
	<div class="panel panel-default">
		<div class="panel-heading">
			<c:if test="${not isSearch}">
				<spring:message code="message.listenAgain" />
			</c:if>
		</div>

		<div class="panel-body">
			<div class="left-panel">
				<span class="photo-song"><img width="65" height="65"
					src="${song.artist.image}" /> </span>
			</div>

			<div class="right-panel" itemscope itemtype="http://schema.org/MusicRecording">
				<div class="song">
					<a target="_blank">${song.name}</a>
				</div>
				<div class="artist" dbpedia-owl="${song.artist.resourceName}">${song.artist.name}</div>
			</div>
		</div>

		<div class="panel-footer custom-footer">
			<button type="button" class="btn btn-default btn-sm" id="playBtn"
				data-song-res="${song.resourceName}" data-song-name="${song.name}"
				data-song-artist-name="${song.artist.name}"
				data-song-artist-img="${song.artist.image}"
				data-song-artist-res="${song.artist.resourceName}"
				data-song-url="${song.url}">
				<span class="glyphicon glyphicon-play-circle"> </span>
			</button>
			<div class="addToPlaylist">
				<button type="button" class="btn btn-default btn-sm" data-toggle="popover"
						title="Add to playlist..." data-html="true" data-placement="bottom"
						onclick="addToPlaylist(this)" 
						dbpedia-owl="${song.resourceName}"
						data-song-name="${song.name}"
						data-song-artist-name="${song.artist.name}"
						data-song-artist-res="${song.artist.resourceName}">
					<span class="glyphicon glyphicon-plus-sign">
					</span>
				</button>
			</div>
		</div>
	</div>
</c:forEach>

<c:forEach items="${recommender.songsFromFacebook}" var="song">
	<div class="panel panel-default">
		<div class="panel-heading">
			<c:if test="${not isSearch}">
				<spring:message code="message.songFromFb" />
			</c:if>
		</div>
		<div class="panel-body">
			<div class="left-panel">
				<span class="photo-song"><img width="65" height="65"
					src="${song.artist.image}" /> </span>
			</div>

			<div class="right-panel" itemscope itemtype="http://schema.org/MusicRecording">
				<div class="song">
					<a target="_blank">${song.name}</a>
				</div>
				<div class="artist" dbpedia-owl="${song.artist.resourceName}">${song.artist.name}</div>
			</div>
		</div>

		<div class="panel-footer custom-footer">
			<button type="button" class="btn btn-default btn-sm" id="playBtn"
					data-song-res="${song.resourceName}" data-song-name="${song.name}"
					data-song-artist-name="${song.artist.name}"
					data-song-artist-img="${song.artist.image}"
					data-song-artist-res="${song.artist.resourceName}"
					data-song-url="${song.url}">
				<span class="glyphicon glyphicon-play-circle"> </span>
			</button>
			<div class="addToPlaylist">
				<button type="button" class="btn btn-default btn-sm" data-toggle="popover"
						title="Add to playlist..." data-html="true" data-placement="bottom"
						onclick="addToPlaylist(this)"
						dbpedia-owl="${song.resourceName}"
						data-song-name="${song.name}"
						data-song-artist-name="${song.artist.name}"
						data-song-artist-res="${song.artist.resourceName}">
					<span class="glyphicon glyphicon-plus-sign">
					</span>
				</button>
			</div>
		</div>
	</div>
</c:forEach>

<script type="text/x-handlebars-template" id="popoverContent">

</script>
<script type="text/javascript">
	function selectPlaylist(id, songResource, songName, artistName, artistResource){
		//BE call to add song to playlist
		$("li[data-id='" + id + "']").addClass('selected');
		$.ajax({
			type: 'POST',
			url: '${pageContext.request.contextPath}/playlist/song',
			data: {
				playlistId: id,
				songId: songResource,
				songName: songName,
				artistName : artistName,
				artistResource : artistResource
			},
			success : function(data) {
			},
			error : function(data) {

			}
		});
	};

	var addToPlaylist = function(_this) {
		var songResource = $(_this).attr("dbpedia-owl");
		var songName = $(_this).attr("data-song-name");
		var artistName = $(_this).attr("data-song-artist-name");
		var artistResource = $(_this).attr("data-song-artist-res");
		
		$.ajax({
			type : 'GET',
			url : '${pageContext.request.contextPath}/playlist/all',
			success : function(data) {
				var template = "<ul>";
				for(var idx=0; idx < data.length; idx++) {
					template = template + "<li class=\"playlist-row\" data-id=\"" + data[idx].id +"\" onclick=\"selectPlaylist('" + data[idx].id + "','" 
					+ songResource + "','" + songName + "','" + artistName + "','" + artistResource + "');\">" + data[idx].name + "</li>";
				}
				template = template + "</ul>";

				$('[data-toggle="popover"]').popover({
					content : template
				});
			},
			error : function(data) {

			}
		});
	};

	$(document).ready(function() {

		$(document).on('click', '#playBtn', function() {

			var songResource = $(this).attr('data-song-res');
			var songName = $(this).attr('data-song-name');
			var artistName = $(this).attr('data-song-artist-name');
			var artistImg = $(this).attr('data-song-artist-img');
			var artistResource = $(this).attr('data-song-artist-res');
			var url = $(this).attr('data-song-url');

			$.ajax({
				type : 'POST',
				url : '${pageContext.request.contextPath}/play',
				data : {
					songResource : songResource,
					songName : songName,
					artistResource : artistResource,
					artistName : artistName,
					artistImage : artistImg
				},
				success : function(data) {
					window.open(url);
				},
				error : function(data) {

				}
			});
		});
	});
</script>