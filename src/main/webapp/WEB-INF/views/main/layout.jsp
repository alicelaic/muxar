<html>
<head>
<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Expires" content="0" />
<%--START: here we should insert all css dependencies-> do not modify bootstrap, add another css file for custom CSS--%>
	<link href="${pageContext.servletContext.contextPath}/resources/css/header.css" rel="stylesheet" type="text/css" />
    <link href="${pageContext.servletContext.contextPath}/resources/css/bootstrap.css" rel="stylesheet" type="text/css" />
    <link href="${pageContext.servletContext.contextPath}/resources/css/body.css" rel="stylesheet" type="text/css" />
	<link href="${pageContext.servletContext.contextPath}/resources/css/main/home.css" rel="stylesheet" type="text/css" />
<%--END--%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

</head>
<body>
<script>
	window.fbAsyncInit = function() {
		FB.init({
			appId      : '1661985780735748',
			xfbml      : true,
			version    : 'v2.5'
		});
	};

	(function(d, s, id){
		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id)) {return;}
		js = d.createElement(s); js.id = id;
		js.src = "//connect.facebook.net/en_US/sdk.js";
		fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));
</script>


		<div class="header"><tiles:insertAttribute name="header" /></div>
        <div><tiles:insertAttribute name="search"/></div>
		<div id="body"><tiles:insertAttribute name="body" /></div>



<%--START: js dependecies, leave bootstrap dependencies untouched -> create new ones--%>
	<script type="text/javascript" src="${pageContext.servletContext.contextPath}/resources/js/jquery-1.11.3.js"></script>
	<script type="text/javascript" src="${pageContext.servletContext.contextPath}/resources/js/bootstrap.js"></script>

<%--END--%>
</body>
</html>