<div class="login-container">
    <img class="logo" src="http://dreamatico.com/data_images/music/music-4.jpg"/>

    <div class="welcome">Welcome to <strong>MUXar</strong></div>

    <li role="presentation" tab-value="history" id="loginBtn">
        <div class="fb-login-button" data-max-rows="1" data-size="large" data-show-faces="false"
             data-auto-logout-link="true" data-scope="public_profile,email,user_events, user_actions.music, user_likes"
             onlogin="checkLoginState();"></div>
    </li>
    <script type="text/javascript"
            src="${pageContext.servletContext.contextPath}/resources/js/facebook/login.js"></script>
</div>
