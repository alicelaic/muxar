<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<c:choose>
	<c:when test="${playlistsPage}">
		<div class="right-panel add-playlist">
			<button type="button" class="btn btn-default pull-right btn-md" id="addNewPlaylist">
				<span class="glyphicon glyphicon-plus"></span>
			</button>
		</div>
	</c:when>
</c:choose>

<c:set value = "${userPlaylists}" var="playlists"/>

	<c:choose>
	    <c:when test="${not empty playlists}">
		
			<c:forEach items="${playlists}" var="playlist">
				<div class="panel panel-default">
					<div class="panel-heading"></div>			
					<div class="panel-body">
					
						<div class="left-panel">
							<span class="photo-song"><img src="http://simpleicon.com/wp-content/uploads/playlist-64x64.png" width="65" height="65"/>
							</span>
						</div>
					
						<div class="right-panel">
							<div class="song">
								<div itemscope itemtype ="http://schema.org/MusicPlaylist">
								<a itemprop="name">${playlist.name}</a><br>
								</div>
							</div>
						</div>
						
					</div>
					
					<div class="panel-footer custom-footer">

						<c:choose>

							<c:when test="${ppublic == true}">
								<button type="button" class="btn btn-default btn-sm">
									<span class="glyphicon glyphicon-save"
									data-playlist-resource="${playlist.id}" onclick="downloadPlaylist(this)"></span>
								</button>
							</c:when>
							<c:otherwise>
								<c:choose>
									<c:when test="${not playlist.shared}">
											<span class="glyphicon glyphicon-lock"></span>
									</c:when>
									<c:otherwise>
											<span class="glyphicon glyphicon-globe"></span>
									</c:otherwise>
								</c:choose>


								<button type="button" class="btn btn-default btn-sm" data-playlist-id="${playlist.id}"
									onclick="viewPlaylist(this)">
									<span class="glyphicon glyphicon-edit"></span>
								</button>

								<button type="button" class="btn btn-default btn-sm" id="deletePlaylist"
										data-playlist-resource="${playlist.id}"
										data-playlist-privacy="${playlist.shared}"
										data-playlist-name="${playlist.name}">
									<span class="glyphicon glyphicon-trash"></span>
								</button>
							</c:otherwise>
						</c:choose>

						
					</div>
					
				</div>
			</c:forEach>
		</c:when>
			
		<c:otherwise>
				
				<div><h4>There are no playlists to be listed.</h4></div>
				
		</c:otherwise>
	</c:choose>
	
	
  <!-- Modal add playlist-->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Create a playlist</h4>
        </div>
        <div class="modal-body">
          <form role="form" action="">
            <div class="form-group">
              <label for="playlistName">Playlist title:</label>
              <input type="text" class="form-control" name="playlistName" id="playlistName">
            </div>
            <div class="radio">
            	Playlist will be: 
      			<label><input type="radio" name="optPrivacy" value="false">Private</label>
      			<label><input type="radio" name="optPrivacy" value="true">Public</label>
    		</div>
            <button type="submit" class="btn btn-primary btn-md" id="savePlaylist">Save</button>
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
        </div>
      </div>
   </div>
 </div>


  <!-- Modal edit playlist-->
  <div class="modal fade" id="myModalEdit" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Tracks</h4>
        </div>
        <div itemscope itemtype="http://schema.org/MusicPlaylist" class="modal-body">
  		<ul id="listSongs" />
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
        </div>
      </div>
   </div>
 </div>


<script type="text/javascript">
$(document).ready(function(){
    $("#addNewPlaylist").click(function(){
        $("#myModal").modal("show");
    });   
});

function downloadPlaylist(_this) {
	var resource = $(_this).attr("data-playlist-resource");
	$.ajax({
		type: 'POST',
		url: '${pageContext.request.contextPath}/playlist/download',
		data: {
			playlistResource: resource
		},
		success: function (data) {
		},
		error: function (data) {
		}
	});

}

$(document).on('click', '#savePlaylist', function(_this) {

	var playlistName = $('input[name="playlistName"]').val();
	var playlistPrivacy = $('input[name="optPrivacy"]:checked').val();

	$.ajax({
		type : 'POST',
		async: false,
		url : '${pageContext.request.contextPath}/playlist/add',
		data : {
			playlistName : playlistName,
			playlistPrivacy : playlistPrivacy

		},
		success : function(data) {
			setTimeout(function(){
				var location = (window.location+'').replace('/add', '');
				window.location.replace(location);
				window.location.reload(true);
            },10);
		},
		error : function(data) {

		}
	});

});

$(document).on('click', '#deletePlaylist', function(_this) {

	var playlistResource = $(this).attr('data-playlist-resource');
	var playlistName = $(this).attr('data-playlist-name');
	var playlistPrivacy = $(this).attr('data-playlist-privacy');
	$.ajax({
		type : 'POST',
		async: false,
		url : '${pageContext.request.contextPath}/playlist/delete',
		data : {
			playlistResource : playlistResource,
			playlistName : playlistName,
			playlistPrivacy : playlistPrivacy
		},
		success : function(data) {
			setTimeout(function(){
				var location = (window.location+'').replace('/delete', '');
				window.location.replace(location);
				window.location.reload(true);
            },10);
		},
		error : function(data) {
		}
	});

});

function deleteSongFromPlaylist(id, songResource){
	
}


var viewPlaylist =  function(_this) {
	var playlistID = $(_this).attr('data-playlist-id');

	$.ajax({
		type : 'GET',
		url : '${pageContext.request.contextPath}/playlist/song?playlistId=' + playlistID,
		success : function(data) {
			var template = "<ul class=\"list-group\">";
			for(var idx=0; idx < data.length; idx++) {
				template = template + "<li itemprop=\"track\" itemscope itemtype=\"http://schema.org/MusicRecording\" "
				+ "class=\"list-group-item\" data-resource=\"" + data[idx].resourceName + " data-playlist-id = \"" + playlistID +"\""
			    + "<div>"
				+ "<a href=\"" + data[idx].url + " target=\"_blank\" itemprop=\"name\"> " + data[idx].name+ "</a>"
				+ "</div>"
				+ "<div class=\"artist\" dbpedia-owl=\"" + data[idx].artist.resourceName + "\" itemprop=\"byArtist\">" + data[idx].artist.name + "</div>"
			    + "</div></li>";
			}
			template = template + "</ul>";

			$("#listSongs").html(template);

			$("#myModalEdit").modal("show");
  
		},
		error : function(data) {

		}
	});
		
}


</script>