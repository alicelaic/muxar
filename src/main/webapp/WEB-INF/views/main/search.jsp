<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<form>
	<div class="row">
		<div class="col-lg-6">
			<div class="input-group">
				<div id="info-sign" class="glyphicon glyphicon-info-sign" data-toggle="popover"
					 data-content="Search for <br/><strong>artist</strong>:Coldplay...<br/><strong>genre</strong>:rock...<br/>or</br> <strong>song name</strong>"
					  title="Hints" data-html="true" data-placement="bottom">
					 </div>

				<input id="inputSearch" type="text" class="form-control"
					placeholder="<spring:message code="message.searchFor" />"> <span class="input-group-btn">
					<button class="btn btn-default" type="button" id="searchBtn"
						name="searchBtn">Go!</button>

				</span>


			</div>
			<!-- /input-group -->
		</div>
		<!-- /.col-lg-6 -->
	</div>
</form>

<script>
	$(document).ready(function(){
		$('#info-sign[data-toggle="popover"]').popover();
	});
	$(document).on('click', '#searchBtn', function() {

		var inputText = document.getElementById("inputSearch").value;

		$.ajax({
			type : 'POST',
			url : '${pageContext.request.contextPath}/search',
			data : {
				searchText : inputText,
			},
			success : function(data) {
				document.open();
				document.write(data);
				document.close();
			},
			error : function(data) {
				document.open();
				document.write(data);
				document.close();
			}
		})
	});
</script>