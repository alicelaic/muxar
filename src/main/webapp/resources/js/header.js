$(document).ready(function () {
  var tabs = {'home': 1, 'playlist': 2, 'history': 3, 'playlist-public': 4};
  var initNavbar = function (path) {
    $("ul.nav li").removeClass("active");
    var pathValues = path.split("/");
    var tabValue = pathValues[pathValues.length -1 ];
    if(tabs[tabValue] == undefined) {
      tabValue = "home";
    }
    $("ul.nav li:nth-child(" + tabs[tabValue] + ")").addClass("active");

  };

  initNavbar(window.location.pathname)
});